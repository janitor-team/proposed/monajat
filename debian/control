Source: monajat
Section: utils
Priority: optional
Maintainer: Debian Islamic Maintainers <debian-islamic-maintainers@lists.alioth.debian.org>
Uploaders: Fadi Al-katout (cutout) <cutout33@gmail.com>, أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@users.sourceforge.net>
Build-Depends: debhelper-compat (= 12)
Build-Depends-Indep: python3-all, gettext, intltool, dh-python
Standards-Version: 4.1.1
Vcs-Git: https://salsa.debian.org/islamic-team/monajat.git
Vcs-Browser: https://salsa.debian.org/islamic-team/monajat
Homepage: https://www.launchpad.net/monajat

Package: python3-monajat
Section: python
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}, monajat-data, libitl0
Breaks: python-monajat
Replaces: python-monajat
Description: Islamic supplications backend
 Monajat is a small application that displays Islamic supplications (azkar) at
 predetermined times.
 .
 This is the Python monajat library needed by monajat front ends.

Package: monajat-data
Architecture: all
Depends: ${misc:Depends}
Description: Islamic supplications database
 Monajat is a small application that displays Islamic supplications (azkar) at
 predetermined times.
 .
 This contains the database of Islamic supplications.

Package: monajat-applet
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}, python3-monajat, python3-gi, python3-gst-1.0
Recommends: python3-dbus, islamic-menus
Breaks: monajat (<= 2)
Replaces: monajat (<= 2)
Description: Islamic supplications tray applet
 Monajat is a small application that displays Islamic supplications (azkar) at
 predetermined times.
 .
 This package contains the desktop tray applet.

Package: monajat-mod
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}, python3-monajat
Description: Islamic supplications console utility
 Monajat is a small application that displays Islamic supplications (azkar) at
 predetermined times.
 .
 This package contains a console application that can be used in motd or in the
 profile.

Package: monajat-screenlet
Architecture: all
Depends: ${misc:Depends}, ${python3:Depends}, python3-monajat
Description: Islamic supplications screenlet
 Monajat is a small application that displays Islamic supplications (azkar) at
 predetermined times.
 .
 This package contains a screenlet.
